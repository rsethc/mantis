/* Totally Standalone Mantis Compiler */

#include "source.h"
#include "lex.h"

#include <stdlib.h>
int lexsrc (Source* src)
{
	Errors errors;
	ErrorsInit(&errors);
	LexState lex;
	LexInit(&lex,src);
	while (1)
	{
		Token token;
		LexToken(&lex,&token,&errors);
		free(token.literal_buf);
		if (token.type == LEX_EOF) break;
	};
	LexQuit(&lex);
	ErrorsQuit(&errors);
	return errors.err_strings_len;
};

int main (int argc, char** argv)
{
	if (argc != 2)
	{
		printf("Usage: mantis sourcefile.man\n");
		return -1;
	};

	FileSource file_src;
	if (!OpenFileSource(&file_src,argv[1]))
	{
		printf("Cannot open \"%s\" for reading\n",argv[1]);
		return -2;
	};
	return lexsrc((Source*)&file_src);
};
