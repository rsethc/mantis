#include "source.h"

#include <string.h>



int ReadNothing (Source* src)
{
	return EOF;
};



int ReadFileSource (FileSource* src)
{
	int ch = fgetc(src->file);
	if (ch == EOF)
	{
		fclose(src->file);
		src->source.read = ReadNothing;
	};
	return ch;
};

void FileSourceFromHandle (FileSource* src, FILE* handle)
{
	src->source.read = (ReadFunc)ReadFileSource;
	src->file = handle;
};

int OpenFileSource (FileSource* src, char* path)
{
	src->source.read = (ReadFunc)ReadFileSource;
	src->file = fopen(path,"rb");
	return src->file != 0;
};



int ReadMemorySource (MemorySource* src)
{
	if (src->read_ptr < src->end_ptr) return *src->read_ptr++;
	else
	{
		if (src->release) src->release(src->buf_start);
		src->source.read = ReadNothing;
		return EOF;
	};
};

void OpenMemorySource (MemorySource* src, char* buf, size_t len, ReleaseFunc release)
{
	src->source.read = (ReadFunc)ReadMemorySource;
	src->buf_start = buf;
	src->read_ptr = buf;
	src->end_ptr = buf + len;
	src->release = release;
};

void OpenStringSource (MemorySource* src, char* str, ReleaseFunc release)
{
	OpenMemorySource(src,str,strlen(str),release);
};
