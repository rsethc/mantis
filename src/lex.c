#include "lex.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>



void LexInit (LexState* lex, Source* src)
{
	lex->source = src;
	lex->canceled_buf = malloc(sizeof(LexChar) * (lex->canceled_size = 1));
	lex->canceled_usage = 0;
	lex->line_number = 1;
	lex->line_position = 1;
};
void LexQuit (LexState* lex)
{
	free(lex->canceled_buf);
};
void CancelChar (LexState* lex, LexChar ch)
{
	if (lex->canceled_usage >= lex->canceled_size)
		lex->canceled_buf = realloc(lex->canceled_buf, sizeof(LexChar) * (lex->canceled_size <<= 1));
	lex->canceled_buf[lex->canceled_usage++] = ch;
	if (ch.value == '\n') lex->line_number--;
	lex->line_position = ch.line_position;
};
LexChar NextChar (LexState* lex)
{
	LexChar ch;
	if (lex->canceled_usage) ch = lex->canceled_buf[--lex->canceled_usage];
	else
	{
		ch.value = lex->source->read(lex->source);
		ch.line_position = lex->line_position;
	};

	if (ch.value == '\n')
	{
		lex->line_position = 1;
		lex->line_number++;
	}
	else lex->line_position++;

	return ch;
};
void SkipSpaces (LexState* lex)
{
	while (1)
	{
		LexChar ch = NextChar(lex);
		if (!isspace(ch.value))
		{
			CancelChar(lex,ch);
			break;
		};
	};
};
char LiteralMatch (Token* token, char* str)
{
	/* IMPORTANT: Assumes strlen(str) == token->literal_len */
	return !memcmp(token->literal_buf,str,token->literal_len);
};
int IdentifyKeyword (Token* token)
{
	switch (token->literal_len)
	{
		case 2:
		if (LiteralMatch(token,"if")) return LEX_IF;
		break;

		case 3:
		if (LiteralMatch(token,"for")) return LEX_FOR;
		break;

		case 4:
		if (LiteralMatch(token,"else")) return LEX_ELSE;
		if (LiteralMatch(token,"true")) return LEX_TRUE;
		break;

		case 5:
		if (LiteralMatch(token,"break")) return LEX_BREAK;
		if (LiteralMatch(token,"while")) return LEX_WHILE;
		if (LiteralMatch(token,"false")) return LEX_FALSE;
		break;

		case 6:
		if (LiteralMatch(token,"return")) return LEX_RETURN;
		if (LiteralMatch(token,"switch")) return LEX_SWITCH;
		break;

		case 8:
		if (LiteralMatch(token,"continue")) return LEX_CONTINUE;
		if (LiteralMatch(token,"function")) return LEX_FUNCTION;
		break;
	};
	return LEX_IDENTIFIER;
};
void ReadIdentifier (LexState* lex, Token* token, char begin)
{
	size_t capacity = 8;
	token->literal_buf = malloc(capacity);
	token->literal_len = 1;
	token->literal_buf[0] = begin;
	while (1)
	{
		LexChar ch = NextChar(lex);

		if (isalnum(ch.value))
		{
			if (token->literal_len >= capacity)
				token->literal_buf = realloc(token->literal_buf, capacity <<= 1);
			token->literal_buf[token->literal_len++] = ch.value;
			continue;
		};

		CancelChar(lex,ch);
		break;
	};

	token->type = IdentifyKeyword(token);
};
void InterpretNumberAsFloating (Token* token, Errors* errors)
{
	int pos = 0;

	token->floating = 0;
	for (; pos < token->literal_len; pos++)
	{
		char ch = token->literal_buf[pos];
		switch (ch)
		{
			case '0' ... '9':
			token->floating *= 10;
			token->floating += ch - '0';
			break;

			case '.':
			pos++;
			goto DOT;

			case 'e': case 'E':
			pos++;
			goto EXPONENT;

			default:
			Error(errors,"invalid floating-point number");
			return;
		};
	};

	DOT:;
	double next_frac = 1;
	for (; pos < token->literal_len; pos++)
	{
		char ch = token->literal_buf[pos];
		switch (ch)
		{
			case '0' ... '9':
			token->floating += (next_frac *= 0.1) * (ch - '0');
			break;

			case 'e': case 'E':
			pos++;
			goto EXPONENT;

			default:
			Error(errors,"invalid floating-point number");
			return;
		};
	};

	EXPONENT:;
	int exponent_sign = 1;
	if (pos < token->literal_len)
	{
		switch (token->literal_buf[pos])
		{
			case '-':
			exponent_sign = -1;
			case '+': // Fall-through intended.
			pos++;
		};
	};
	int exponent_value = 0;
	for (; pos < token->literal_len; pos++)
	{
		char ch = token->literal_buf[pos];
		switch (ch)
		{
			case '0' ... '9':
			exponent_value *= 10;
			exponent_value += ch - '0';
			break;

			default:
			Error(errors,"invalid floating-point number");
			return;
		};
	};
	token->floating *= pow(10,exponent_value * exponent_sign);
};
void InterpretNumberAsDecimal (Token* token, Errors* errors)
{
	for (int i = 0; i < token->literal_len; i++)
	{
		token->integer *= 10;
		char ch = token->literal_buf[i];
		switch (ch)
		{
			case '0' ... '9':
			token->integer += ch - '0';
			break;

			default:
			Error(errors,"invalid decimal integer");
			return;
		};
	};
};
void InterpretNumberAsOctal (Token* token, Errors* errors)
{
	for (int i = 1; i < token->literal_len; i++)
	{
		token->integer <<= 3;
		char ch = token->literal_buf[i];
		switch (ch)
		{
			case '0' ... '7':
			token->integer |= ch - '0';
			break;

			default:
			Error(errors,"invalid octal integer");
			return;
		};
	};
};
void InterpretNumberAsHex (Token* token, Errors* errors)
{
	for (int i = 2; i < token->literal_len; i++)
	{
		token->integer <<= 4;
		char ch = token->literal_buf[i];
		switch (ch)
		{
			case 'a' ... 'f':
			token->integer |= ch - 'a' + 10;
			break;

			case 'A' ... 'F':
			token->integer |= ch - 'A' + 10;
			break;

			case '0' ... '9':
			token->integer |= ch - '0';
			break;

			default:
			Error(errors,"invalid hex integer");
			return;
		};
	};
};
void InterpretNumberAsBinary (Token* token, Errors* errors)
{
	for (int i = 2; i < token->literal_len; i++)
	{
		token->integer <<= 1;
		char ch = token->literal_buf[i];
		if (ch == '1') token->integer |= 1;
		else if (ch != '0')
		{
			Error(errors,"invalid binary integer");
			return;
		};
	};
};
void InterpretNumber (Token* token, Errors* errors)
{
	for (int i = 0; i < token->literal_len; i++)
	{
		char ch = token->literal_buf[i];
		if (ch == '.' || ch == 'e' || ch == 'E')
		{
			token->type = LEX_FLOATING;
			InterpretNumberAsFloating(token,errors);
			return;
		};
	};
	token->type = LEX_INTEGER;
	token->integer = 0;
	char first = token->literal_buf[0];
	if (first == '0' && token->literal_len >= 2)
	{
		char second = token->literal_buf[1];
		if (second == 'x') InterpretNumberAsHex(token,errors);
		else if (second == 'b') InterpretNumberAsBinary(token,errors);
		else InterpretNumberAsOctal(token,errors);
	}
	else InterpretNumberAsDecimal(token,errors);
};
void ReadNumber (LexState* lex, Token* token, char begin, Errors* errors)
{
	size_t capacity = 8;
	token->literal_buf = malloc(capacity);
	token->literal_len = 1;
	token->literal_buf[0] = begin;
	char prev = 0;
	while (1)
	{
		LexChar ch = NextChar(lex);
		switch (ch.value)
		{
			case '+': case '-':
			if (prev != 'e' && prev != 'E') break;
			/*  If previous *was* 'e' or 'E' then just fall through
				and treat the '+' or '-' as part of the number.  */
			case '0' ... '9':
			case 'a' ... 'f':
			case 'A' ... 'F':
			case '.':
			case 'x':
			//case 'b': /* Already covered by 'a' ... 'f' */
			prev = ch.value;
			if (token->literal_len >= capacity)
				token->literal_buf = realloc(token->literal_buf, capacity <<= 1);
			token->literal_buf[token->literal_len++] = ch.value;
			continue;
		};
		CancelChar(lex,ch);
		break;
	};

	if (token->literal_len == 1 && token->literal_buf[0] == '.') token->type = LEX_DOT;
	else InterpretNumber(token,errors);
};
void AppendStringChar (Token* token, int* strbuf_size, char ch)
{
	if (token->string_len >= *strbuf_size)
		token->string_buf = realloc(token->string_buf, *strbuf_size <<= 1);
	token->string_buf[token->string_len++] = ch;
};

void InterpretStringLiteral (Token* token, Errors* errors)
{
	int strbuf_size = 8;
	token->string_buf = malloc(strbuf_size);
	token->string_len = 0;
	char start = token->literal_buf[0];
	int inner_len = token->literal_len - 1;
	if (token->literal_buf[inner_len] != start)
	{
		Error(errors,"unterminated string literal");
		return;
	};
	for (int i = 1; i < inner_len; )
	{
		char ch = token->literal_buf[i++];
		if (ch == '\\')
		{
			if (i < inner_len)
			{
				ch = token->literal_buf[i++];
				if (ch != start) switch (ch)
				{
					case '\\': break;
					case 'n': ch = '\n'; break;
					case 'f': ch = '\f'; break;
					case 'r': ch = '\r'; break;
					case 'b': ch = '\b'; break;
					case 't': ch = '\t'; break;
					case '0': ch = 0; break;

					case 'x': // Expect two hex characters, for a raw byte.
					{
						Token ntok;
						ntok.literal_buf = token->literal_buf + i;
						ntok.literal_len = 2;
						if ((i += 2) > inner_len)
						{
							Error(errors,"incomplete hex escape sequence");
							return;
						};
						ntok.integer = 0; ntok.literal_buf -= 2; ntok.literal_len += 2;
						InterpretNumberAsHex(&ntok,errors);
						ch = ntok.integer;
					};
					break;

					case 'u': // Expect three hex characters, for a unicode code point.
					{
						Token ntok;
						ntok.literal_buf = token->literal_buf + i;
						ntok.literal_len = 0;
						while (1)
						{
							if (i >= inner_len)
							{
								Error(errors,"incomplete unicode escape sequence");
								return;
							};
							if (token->literal_buf[i++] == ';') break;
							ntok.literal_len++;
						};
						ntok.integer = 0; ntok.literal_buf -= 2; ntok.literal_len += 2;
						InterpretNumberAsHex(&ntok,errors);
						if (ntok.integer <= 0x7F)
						{
							ch = ntok.integer;
							break;
						}
						else if (ntok.integer <= 0x7FF)
						{
							AppendStringChar(token,&strbuf_size, 0xC0 | (ntok.integer >> 6));
							AppendStringChar(token,&strbuf_size, 0x80 | (ntok.integer & 0x3F));
						}
						else if (ntok.integer < 0xFFFF)
						{
							AppendStringChar(token,&strbuf_size, 0xE0 | (ntok.integer >> 12));
							AppendStringChar(token,&strbuf_size, 0x80 | ((ntok.integer >> 6) & 0x3F));
							AppendStringChar(token,&strbuf_size, 0x80 | (ntok.integer & 0x3F));
						}
						else
						{
							AppendStringChar(token,&strbuf_size, 0xF0 | (ntok.integer >> 18));
							AppendStringChar(token,&strbuf_size, 0x80 | ((ntok.integer >> 12) & 0x3F));
							AppendStringChar(token,&strbuf_size, 0x80 | ((ntok.integer >> 6) & 0x3F));
							AppendStringChar(token,&strbuf_size, 0x80 | (ntok.integer & 0x3F));
						};
						continue;
					};

					default:
						Error(errors,"invalid escape sequence");
						return;
				};
			}
			else
			{
				Error(errors,"incomplete escape sequence in string literal");
				return;
			};
		};
		AppendStringChar(token,&strbuf_size,ch);
	};
};
void ReadStringLiteral (LexState* lex, Token* token, char begin, Errors* errors)
{
	token->type = LEX_STRLIT;
	size_t capacity = 8;
	token->literal_buf = malloc(capacity);
	token->literal_len = 1;
	token->literal_buf[0] = begin;
	char prev = 0;
	while (1)
	{
		LexChar ch = NextChar(lex);

		if (ch.value == EOF || ch.value == '\n')
		{
			CancelChar(lex,ch);
			break;
		};

		if (token->literal_len >= capacity)
			token->literal_buf = realloc(token->literal_buf, capacity <<= 1);
		token->literal_buf[token->literal_len++] = ch.value;

		if (ch.value == begin && prev != '\\') break;

		prev = ch.value;
	};
	InterpretStringLiteral(token,errors);
};
#define SymLen2of2 { token->literal_len = 2; }
#define SymLen1of2 { token->literal_len = 1; CancelChar(lex,second); }
#define SymLen3of3 { token->literal_len = 3; }
#define SymLen2of3 { token->literal_len = 2; CancelChar(lex,third); }
#define SymLen1of3 { token->literal_len = 1; CancelChar(lex,third); CancelChar(lex,second); }
int IdentifySymbol (LexState* lex, Token* token, LexChar first)
{
	LexChar second, third;
	token->literal_buf = malloc(8);

	// Handle operators that are up to one character long.
	token->literal_buf[0] = first.value;
	token->literal_len = 1;
	switch (first.value)
	{
		case ',': return LEX_COMMA;
		case '[': return LEX_OPENBRACKET;
		case ']': return LEX_CLOSEBRACKET;
		case '{': return LEX_OPENBRACE;
		case '}': return LEX_CLOSEBRACE;
		case '(': return LEX_OPENPAREN;
		case ')': return LEX_CLOSEPAREN;
		case '?': return LEX_QUESTION;
		case ':': return LEX_COLON;
		case ';': return LEX_SEMICOLON;
		case '~': return LEX_BITNOT;
		case '$': return LEX_JSON;
		case '#': return LEX_COUNT;

		// Handle operators that are up to two characters long.
		default:
		token->literal_buf[1] = (second = NextChar(lex)).value;
		switch (first.value)
		{
			case '+': /* ++ += + */
			switch (second.value)
			{
				case '+': SymLen2of2; return LEX_PLUSPLUS;
				case '=': SymLen2of2; return LEX_PLUSEQUALS;
				default: SymLen1of2; return LEX_PLUS;
			};
			case '-': /* -- -= - */
			switch (second.value)
			{
				case '-': SymLen2of2; return LEX_MINUSMINUS;
				case '=': SymLen2of2; return LEX_MINUSEQUALS;
				default: SymLen1of2; return LEX_MINUS;
			};
			case '*': /* *= * */
			switch (second.value)
			{
				case '=': SymLen2of2; return LEX_TIMESEQUALS;
				default: SymLen1of2; return LEX_TIMES;
			};
			case '/': /* /= / */
			switch (second.value)
			{
				case '=': SymLen2of2; return LEX_DIVEQUALS;
				default: SymLen1of2; return LEX_DIV;
			};
			case '!': /* != ! */
			switch (second.value)
			{
				case '=': SymLen2of2; return LEX_UNEQUAL;
				default: SymLen1of2; return LEX_LOGICNOT;
			};
			case '=': /* == = */
			switch (second.value)
			{
				case '=': SymLen2of2; return LEX_EQUAL;
				default: SymLen1of2; return LEX_ASSIGN;
			};

			// Handle operators that are up to three characters long.
			default:
			token->literal_buf[2] = (third = NextChar(lex)).value;
			switch (first.value)
			{
				case '|': /* ||= || |= | */
				switch (second.value)
				{
					case '|':
					switch (third.value)
					{
						case '=': SymLen3of3; return LEX_LOGICOREQUALS;
						default: SymLen2of3; return LEX_LOGICOR;
					};
					case '=': SymLen2of3; return LEX_BITOREQUALS;
					default: SymLen1of3; return LEX_BITOR;
				};
				case '&': /* &&= && &= & */
				switch (second.value)
				{
					case '&':
					switch (third.value)
					{
						case '=': SymLen3of3; return LEX_LOGICANDEQUALS;
						default: SymLen2of3; return LEX_LOGICAND;
					};
					case '=': SymLen2of3; return LEX_BITANDEQUALS;
					default: SymLen1of3; return LEX_BITAND;
				};
				case '^': /* ^^= ^^ ^= ^ */
				switch (second.value)
				{
					case '^':
					switch (third.value)
					{
						case '=': SymLen3of3; return LEX_LOGICXOREQUALS;
						default: SymLen2of3; return LEX_LOGICXOR;
					};
					case '=': SymLen2of3; return LEX_BITXOREQUALS;
					default: SymLen1of3; return LEX_BITXOR;
				};
				case '>': /* >>= >> >= > */
				switch (second.value)
				{
					case '>':
					switch (third.value)
					{
						case '=': SymLen3of3; return LEX_RIGHTSHIFTEQUALS;
						default: SymLen2of3; return LEX_RIGHTSHIFT;
					};
					case '=': SymLen2of3; return LEX_GREATEREQUAL;
					default: SymLen1of3; return LEX_GREATER;
				};
				case '<': /* <<= << <= < */
				switch (second.value)
				{
					case '<':
					switch (third.value)
					{
						case '=': SymLen3of3; return LEX_LEFTSHIFTEQUALS;
						default: SymLen2of3; return LEX_LEFTSHIFT;
					};
					case '=': SymLen2of3; return LEX_LESSEQUAL;
					default: SymLen1of3; return LEX_LESS;
				};

				// Handle unrecognized operator.
				default: SymLen1of3; return LEX_INVALID;
			};
		};
	};
};
void LexToken (LexState* lex, Token* token, Errors* errors)
{
	SkipSpaces(lex);
	token->line_number = lex->line_number;
	token->line_position = lex->line_position;

	LexChar first = NextChar(lex);
	if (isalpha(first.value))
		ReadIdentifier(lex,token,first.value);
	else if (isdigit(first.value) || first.value == '.')
		ReadNumber(lex,token,first.value,errors);
	else
	{
		switch (first.value)
		{
			case EOF:
			token->type = LEX_EOF;
			token->literal_buf = malloc(0);
			token->literal_len = 0;
			break;

			case '"': case '\'': case '`':
			ReadStringLiteral(lex,token,first.value,errors);
			break;

			default:
			token->type = IdentifySymbol(lex,token,first);
		};
	};
};
char* TokenNames [] =
{
	"EOF",
	"INVALID",
	"IDENTIFIER",

	// Keywords
	"IF",
	"FOR",
	"ELSE",
	"TRUE",
	"BREAK",
	"WHILE",
	"FALSE",
	"RETURN",
	"SWITCH",
	"CONTINUE",
	"FUNCTION",
	"CLASS",
	"FORGET",

	"INTEGER",
	"FLOATING",
	"STRLIT",
	"DOT",
	"COMMA",
	"OPENBRACKET",
	"CLOSEBRACKET",
	"OPENBRACE",
	"CLOSEBRACE",
	"OPENPAREN",
	"CLOSEPAREN",
	"QUESTION",
	"COLON",
	"SEMICOLON",
	"PLUSPLUS",
	"PLUSEQUALS",
	"PLUS",
	"MINUSMINUS",
	"MINUSEQUALS",
	"MINUS",
	"TIMESEQUALS",
	"TIMES",
	"DIVEQUALS",
	"DIV",
	"LOGICOREQUALS",
	"LOGICOR",
	"BITOREQUALS",
	"BITOR",
	"LOGICANDEQUALS",
	"LOGICAND",
	"BITANDEQUALS",
	"BITAND",
	"LOGICXOREQUALS",
	"LOGICXOR",
	"BITXOREQUALS",
	"BITXOR",
	"UNEQUAL",
	"LOGICNOT",
	"BITNOT",
	"EQUAL",
	"ASSIGN",
	"RIGHTSHIFTEQUALS",
	"RIGHTSHIFT",
	"GREATEREQUAL",
	"GREATER",
	"LEFTSHIFTEQUALS",
	"LEFTSHIFT",
	"LESSEQUAL",
	"LESS",
	"JSON",
	"COUNT",
};
char* TokenName (int type)
{
	if (sizeof(TokenNames) / sizeof(*TokenNames) != N_TOKEN_ENUMS)
		return "token names are broken";
	if (type >= 0 && type < (sizeof(TokenNames)/sizeof(*TokenNames)))
		return TokenNames[type];
	return "???";
};
