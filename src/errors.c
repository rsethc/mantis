#include "errors.h"
#include <stdlib.h>
#include <stdio.h>

void ErrorsInit (Errors* errors)
{
	errors->err_strings_buf = malloc(sizeof(char*) * (errors->err_strings_size = 8));
	errors->err_strings_len = 0;
};
void ErrorsQuit (Errors* errors)
{
	// At the moment Error is called with string constants, not heap allocated strings.
	// So definitely do not attempt to free those.
	/*for (int i = 0; i < errors->err_strings_len; i++)
		free(errors->err_strings_buf[i]);*/
	free(errors->err_strings_buf);
};
void Error (Errors* errors, char* msg)
{
	if (errors->err_strings_len >= errors->err_strings_size)
		errors->err_strings_buf = realloc(errors->err_strings_buf, sizeof(char*) * (errors->err_strings_size <<= 1));
	errors->err_strings_buf[errors->err_strings_len++] = msg;
};
