#include "parse.h"

typedef struct ParseState ParseState;
struct ParseState
{
	LexState* lex;
	Errors* errors;
	Token* canceled_buf;
	int canceled_size, canceled_usage;
	Program* program;
};
void InitParseState (ParseState* state, LexState* lex, Errors* errors, Program* program)
{
	state->lex = lex;
	state->errors = errors;
	state->program = program;
	state->canceled_buf = malloc(sizeof(Token) * (state->canceled_size = 1));
	state->canceled_usage = 0;
};
void QuitParseState (ParseState* state)
{
	free(state->canceled_buf);
};
void NextToken (ParseState* state, Token* token)
{
	if (state->canceled_usage) *token = state->canceled_buf[--state->canceled_usage];
	else LexToken(state->lex,token,state->errors);
};
void CancelToken (ParseState* state, Token* token)
{
	if (state->canceled_usage >= state->canceled_size)
		state->canceled_buf = realloc(state->canceled_buf, sizeof(Token) * (state->canceled_size <<= 1));
	state->canceled_buf[state->canceled_usage++] = *token;
};
void PrintLiteral (Token* token)
{
	for (int i = 0; i < token->literal_len; i++)
		putchar(token->literal_buf[i]);
};



/*
	Type checking should occur during ascent as the lower calls start returning to their callers.
	Same goes for constant folding.
*/

void ExprLeaf (ParseState* state)
{
	Token literal_or_varname; // todo: need to also detect parens after to mean func call
	NextToken(state,&literal_or_varname);
	if (literal_or_varname.type == LEX_IDENTIFIER /* also detect the other kinds of literals */)
	{
		printf("var[");
		PrintLiteral(&literal_or_varname);
		printf("]");
	}
	else
	{
		printf("wtf?");
	};
};

#define OP_ID          0xFFFF
#define OP_UNARY_PRE  0x10000
#define OP_UNARY_POST 0x20000
#define OPGROUP_LEFT  0x40000
int ParseLevels [] = {
OPGROUP_LEFT | // this group collapses to the left
	LEX_ASSIGN,
	LEX_PLUSEQUALS,
	LEX_MINUSEQUALS,
	LEX_TIMESEQUALS,
	LEX_DIVEQUALS,
	LEX_BITANDEQUALS,
	LEX_BITOREQUALS,
	LEX_BITXOREQUALS,
	LEX_LOGICANDEQUALS,
	LEX_LOGICOREQUALS,
	LEX_LOGICXOREQUALS,
LEX_EOF,

	LEX_PLUS,
	LEX_MINUS,
LEX_EOF,

	LEX_TIMES,
	LEX_DIV,
LEX_EOF,

	// Empty group to signify no more levels.
LEX_EOF
};

void ExprLevel (ParseState* state, int* level)
{
	if (*level == LEX_EOF) ExprLeaf(state);
	else
	{
		/*
			Look for any UNARY_PRE operator token before first next-level call.
			For UNARY_POST simply don't do the second next-level call.

			Need to figure out how to handle consecutive same-level operators:
				a = b * c / d * e; <- All on the same level. Loops?
		*/


		int* next_level = level + 1;
		while (*next_level != LEX_EOF) next_level++;
		next_level++;

		printf("(");
		ExprLevel(state,next_level);

		Token maybe_op;
		NextToken(state,&maybe_op);

		while (*level != LEX_EOF)
		{
			if (maybe_op.type == (*level & OP_ID))
			{
				printf(" %s ",TokenName(maybe_op.type));

				ExprLevel(state,next_level);
				printf(")");

				/* constant-fold or make non-leaf node */

				return;
			};
			level++;
		};

		printf(")");
		CancelToken(state,&maybe_op);

		/* make leaf node */
	};
};

void ReadExpr (ParseState* state)
{
	Token maybe_openparen;
	NextToken(state,&maybe_openparen);
	if (maybe_openparen.type == LEX_OPENPAREN)
	{
		ReadExpr(state);
		Token shouldbe_closeparen;
		NextToken(state,&shouldbe_closeparen);
		if (shouldbe_closeparen.type != LEX_CLOSEPAREN)
		{
			printf("expected close paren\n");
			return;
		};
	}
	else
	{
		CancelToken(state,&maybe_openparen);
		ExprLevel(state,ParseLevels);
	};
};

void ReadDeclaration (ParseState* state, Token typename)
{
	while (1)
	{
		Token varname;
		NextToken(state,&varname);
		if (varname.type != LEX_IDENTIFIER)
		{
			printf("expected identifier\n");
			return;
		};

		// Variable Declaration
		printf("Variable declaration. Type: \"");
		PrintLiteral(&typename);
		printf("\" Name: \"");
		PrintLiteral(&varname);
		printf("\"\n");



		Token ending;
		NextToken(state,&ending);
		if (varname.type == LEX_SEMICOLON) break;
		else if (varname.type == LEX_COMMA) continue;
		else if (varname.type != LEX_ASSIGN)
		{
			printf("expected semicolon, comma, or assign\n");
			return;
		};

		// Initial assignment
		ReadExpr(state);
		// & set var to result.

		NextToken(state,&ending);
		if (varname.type == LEX_SEMICOLON) break;
		else if (varname.type != LEX_COMMA)
		{
			printf("expected semicolon or comma\n");
			return;
		};
	};
};

void ReadForget (ParseState* state)
{
};

void ReadIf (ParseState* state)
{
};

void ReadFor (ParseState* state)
{
};

void ReadWhile (ParseState* state)
{
};

void ReadFunction (ParseState* state)
{
	// function funcname (type1 name1, type2 name2) -> rettype { block }
	// function funcname { block }
};

void ReadClass (ParseState* state, Token* inherit_from /* may be NULL */)
{
	// class A { ... } // inherit_from = NULL
	// A class B { ... } // inherit_from = the identifier A

};

Program* ParseBlock (ParseState* state, int end_type)
{
	// For the program itself, end_type will be LEX_EOF.
	// For inner code blocks, end_type will be LEX_CLOSEBRACE.

	while (1)
	{
		Token first;
		NextToken(state,&first);
		if (first.type == end_type) break;
		else if (first.type == LEX_IDENTIFIER)
		{
			Token second;
			NextToken(state,&second);
			CancelToken(state,&second);
			if (second.type == LEX_IDENTIFIER) ReadDeclaration(state,first);
			else if (second.type == LEX_CLASS) ReadClass(state,&first);
			else
			{
				CancelToken(state,&first);
				printf("<");
				ReadExpr(state);
				printf(">");
				Token semicolon;
				NextToken(state,&semicolon);
				if (semicolon.type != LEX_SEMICOLON)
					printf("expected semicolon");
			};
		}
		else if (first.type == LEX_FORGET) ReadForget(state);
		else if (first.type == LEX_OPENBRACE) ParseBlock(state,LEX_CLOSEBRACE);
		else if (first.type == LEX_IF) ReadIf(state);
		else if (first.type == LEX_WHILE) ReadWhile(state);
		else if (first.type == LEX_FOR) ReadFor(state);
		else if (first.type == LEX_FUNCTION) ReadFunction(state);
		else if (first.type == LEX_CLASS) ReadClass(state,NULL);
		else if (first.type == LEX_SEMICOLON); // ignore it entirely
		else
		{
			printf("<");
			ReadExpr(state);
			printf(">");
			Token semicolon;
			NextToken(state,&semicolon);
			if (semicolon.type != LEX_SEMICOLON)
				printf("expected semicolon");
		};
	};
};

Program* Parse (LexState* lex, Errors* errors)
{
	// Set up the Program object and start recursive descent with ParseBlock.

	Program* program = malloc(sizeof(Program));

	ParseState state;
	InitParseState(&state,lex,errors,program);

	ParseBlock(&state,LEX_EOF);

	QuitParseState(&state);

	if (errors->err_strings_len)
	{
		free(program);
		return NULL;
	}
	else return program;
};
