#include "source.h"
#include "lex.h"
#include "parse.h"

#include <stdlib.h>

void printsrc (Source* src)
{
	printf("{");
	while (1)
	{
		int cc = src->read(src);
		if (cc == EOF) break;
		putchar(cc);
	};
	printf("}\n");
};
#include <string.h>
void printsrc_tests ()
{
	printf("Static string test: ");
	MemorySource str_src;
	OpenStringSource(&str_src,"Hello world, from a string constant.",NULL);
	printsrc((Source*)&str_src);

	printf("Allocated string test: ");
	char* from = "Hello world, from a heap-allocated string.";
	size_t from_len = strlen(from);
	void* buf = malloc(from_len);
	memcpy(buf,from,from_len);
	OpenMemorySource(&str_src,buf,from_len,free);
	printsrc((Source*)&str_src);

	printf("File test: ");
	FileSource file_src;
	OpenFileSource(&file_src,"program.man");
	printsrc((Source*)&file_src);
};
void lexsrc (Source* src)
{
	printf("{\n");
	Errors errors;
	ErrorsInit(&errors);
	LexState lex;
	LexInit(&lex,src);
	while (1)
	{
		Token token;
		LexToken(&lex,&token,&errors);

		printf("\t%d:%d (%s) ",token.line_number,token.line_position,TokenName(token.type));
		for (int i = 0; i < token.literal_len; i++) putchar(token.literal_buf[i]);

		if (token.type == LEX_INTEGER) printf(" -> %lld",token.integer);
		else if (token.type == LEX_FLOATING) printf(" -> %lf",token.floating);
		else if (token.type == LEX_STRLIT)
		{
			printf(" -> ");
			for (int i = 0; i < token.string_len; i++) putchar(token.string_buf[i]);
			free(token.string_buf);
		};

		putchar('\n');
		free(token.literal_buf);

		if (token.type == LEX_EOF) break;
	};
	LexQuit(&lex);
	printf("}\n");
	printf("%d errors\n",(int)errors.err_strings_len);
	for (int i = 0; i < errors.err_strings_len; i++)
		printf("\t%s\n",errors.err_strings_buf[i]);
	ErrorsQuit(&errors);
};
void lexsrc_tests ()
{
	printf("Static string test: ");
	MemorySource str_src;
	OpenStringSource(&str_src,"Hello world, from a string constant.",NULL);
	lexsrc((Source*)&str_src);

	printf("Allocated string test: ");
	char* from = "Hello world, from a heap-allocated string.";
	size_t from_len = strlen(from);
	void* buf = malloc(from_len);
	memcpy(buf,from,from_len);
	OpenMemorySource(&str_src,buf,from_len,free);
	lexsrc((Source*)&str_src);

	printf("File test: ");
	FileSource file_src;
	OpenFileSource(&file_src,"program.man");
	lexsrc((Source*)&file_src);
};
void parsesrc (Source* src)
{
	Errors errors;
	ErrorsInit(&errors);
	LexState lex;
	LexInit(&lex,src);
	Program* program = Parse(&lex,&errors);
	if (program)
	{
		printf("Compilation succeeded.\n");
	}
	else
	{
		printf("Compilation failed.\n");
	};
	LexQuit(&lex);
	printf("%d errors\n",(int)errors.err_strings_len);
	for (int i = 0; i < errors.err_strings_len; i++)
		printf("\t%s\n",errors.err_strings_buf[i]);
	ErrorsQuit(&errors);
};
void parsesrc_tests ()
{
	/*MemorySource str_src;
	OpenStringSource(&str_src,"int x; int y; int; int b = int j k ",NULL);
	parsesrc((Source*)&str_src);*/

	printf("File test: ");
	FileSource file_src;
	OpenFileSource(&file_src,"program.man");
	parsesrc((Source*)&file_src);
};
int main ()
{
	//printsrc_tests();
	//lexsrc_tests();
	parsesrc_tests();
	return 0;
};
