$(shell mkdir -p lib)
$(shell mkdir -p bin)

CFLAGS +=\
	-fPIC\
	-Werror=implicit-function-declaration\
	-Werror=incompatible-pointer-types\
	#-O2\
	#-s\
	#-fomit-frame-pointer\

INCLUDES = -I /usr/local/include/ -I /usr/include/ -I include/

LIBRARIES =\
	-lmantis\
	-lm\

LIBDIR = -L /usr/local/lib/ -L /usr/lib/ -L lib/

OBJECTS =\
	$(patsubst %.c,%.o,$(shell find src -iname '*.c'))

all: lib/libmantis.a
	$(CC) $(CFLAGS) -o bin/testing $(INCLUDES) $(LIBDIR) testing.c $(LIBRARIES)
	$(CC) $(CFLAGS) -o bin/tsmc $(INCLUDES) $(LIBDIR) tsmc.c $(LIBRARIES)

lib/libmantis.a: $(OBJECTS)
	ar -cr $@ $^

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	rm -rf pkg
	@rm -rvf $(shell find . -iname '*.o')

package: all
	rm -rf pkg
	rm -f mantis-linux.zip
	mkdir -p pkg/include pkg/lib pkg/bin
	cp -r include pkg
	cp -r lib pkg
	cp -r bin pkg
	zip -r mantis-linux.zip pkg

windows:
	CC=gcc.exe make
	# Expectations:
		# Setup WSL
		# Use Linux make
		# Install Windows mingw-w64
		# bin/ folder of mingw-w64 in Windows PATH
	# If linking errors, first try `make clean`.
