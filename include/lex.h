#ifndef LEX_H_INCLUDED
#define LEX_H_INCLUDED

#include "source.h"
#include "errors.h"

typedef struct LexChar LexChar;
struct LexChar
{
	int value, line_position;
};
typedef struct LexState LexState;
struct LexState
{
	Source* source;
	LexChar* canceled_buf;
	size_t canceled_size, canceled_usage;
	int line_number, line_position;
};
void LexInit (LexState* lex, Source* src);
void LexQuit (LexState* lex);
enum
{
	LEX_EOF,
	LEX_INVALID,
	LEX_IDENTIFIER,

	// Keywords
	LEX_IF,
	LEX_FOR,
	LEX_ELSE,
	LEX_TRUE,
	LEX_BREAK,
	LEX_WHILE,
	LEX_FALSE,
	LEX_RETURN,
	LEX_SWITCH,
	LEX_CONTINUE,
	LEX_FUNCTION,
	LEX_CLASS, 
	LEX_FORGET, // forget x,y;

	LEX_INTEGER,
	LEX_FLOATING,
	LEX_STRLIT,
	LEX_DOT,
	LEX_COMMA,
	LEX_OPENBRACKET,
	LEX_CLOSEBRACKET,
	LEX_OPENBRACE,
	LEX_CLOSEBRACE,
	LEX_OPENPAREN,
	LEX_CLOSEPAREN,
	LEX_QUESTION,
	LEX_COLON,
	LEX_SEMICOLON,
	LEX_PLUSPLUS,
	LEX_PLUSEQUALS,
	LEX_PLUS,
	LEX_MINUSMINUS,
	LEX_MINUSEQUALS,
	LEX_MINUS,
	LEX_TIMESEQUALS,
	LEX_TIMES,
	LEX_DIVEQUALS,
	LEX_DIV,
	LEX_LOGICOREQUALS,
	LEX_LOGICOR,
	LEX_BITOREQUALS,
	LEX_BITOR,
	LEX_LOGICANDEQUALS,
	LEX_LOGICAND,
	LEX_BITANDEQUALS,
	LEX_BITAND,
	LEX_LOGICXOREQUALS,
	LEX_LOGICXOR,
	LEX_BITXOREQUALS,
	LEX_BITXOR,
	LEX_UNEQUAL,
	LEX_LOGICNOT,
	LEX_BITNOT,
	LEX_EQUAL,
	LEX_ASSIGN,
	LEX_RIGHTSHIFTEQUALS,
	LEX_RIGHTSHIFT,
	LEX_GREATEREQUAL,
	LEX_GREATER,
	LEX_LEFTSHIFTEQUALS,
	LEX_LEFTSHIFT,
	LEX_LESSEQUAL,
	LEX_LESS,
	LEX_JSON,
	LEX_COUNT,

	N_TOKEN_ENUMS
};
typedef struct Token Token;
struct Token
{
	int type;
	char* literal_buf;
	size_t literal_len;
	int line_number, line_position;

	/* Actual values for LEX_INTEGER, LEX_FLOATING, LEX_STRLIT */
	union
	{
		struct
		{
			char* string_buf;
			size_t string_len;
		};
		long long integer;
		double floating;
	};
};
void LexToken (LexState* lex, Token* token, Errors* errors);
char* TokenName (int type);

#endif // LEX_H_INCLUDED
