#ifndef ERRORS_H_INCLUDED
#define ERRORS_H_INCLUDED

#include <stdlib.h>

typedef struct Errors Errors;
struct Errors
{
	char** err_strings_buf;
	size_t err_strings_len, err_strings_size;
};
void ErrorsInit (Errors* errors);
void ErrorsQuit (Errors* errors);
void Error (Errors* error, char* msg);

#endif // ERRORS_H_INCLUDED
