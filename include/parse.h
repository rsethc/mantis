#ifndef PARSE_H_INCLUDED
#define PARSE_H_INCLUDED

#include "lex.h"

typedef struct Program Program;
struct Program
{
	/* Probably these things:
		Program code intermediate representation
		String constants
		Names of externally bound functions
	*/
	/* These things shouldn't need to be stored permanently:
		Names of variables, struct definitions, function definitions
	*/
};

Program* Parse (LexState* lex, Errors* errors);

#endif // PARSE_H_INCLUDED
