#ifndef SOURCE_H_INCLUDED
#define SOURCE_H_INCLUDED

typedef struct Source Source;
typedef int (*ReadFunc) (Source* source);
struct Source
{
	ReadFunc read;
};

#include <stdio.h>
typedef struct FileSource FileSource;
struct FileSource
{
	Source source;
	FILE* file;
};
void FileSourceFromHandle (FileSource* src, FILE* handle);
int OpenFileSource (FileSource* src, char* path);

typedef void (*ReleaseFunc) (void* buf_start);
typedef struct MemorySource MemorySource;
struct MemorySource
{
	Source source;
	char* read_ptr;
	char* end_ptr;
	char* buf_start;
	ReleaseFunc release;
};
void OpenMemorySource (MemorySource* src, char* buf, size_t len, ReleaseFunc release);
void OpenStringSource (MemorySource* src, char* str, ReleaseFunc release);

#endif // SOURCE_H_INCLUDED
